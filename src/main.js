// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import vueResource from 'vue-resource'
import layer from 'vue-layer'
import api from 'api/index'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import'../static/ueditor/ueditor.config.js'
import'../static/ueditor/ueditor.all.js'
import'../static/ueditor/lang/zh-cn/zh-cn.js'
import'../static/ueditor/ueditor.parse.min.js'
import http from 'axios'
import 'babel-polyfill'

Vue.prototype.$http = http
Vue.use(ElementUI);
  // 将API方法绑定到全局吧
Vue.prototype.$http = http
Vue.prototype.$api = api
Vue.prototype.$layer = layer(Vue);
Vue.use(vueResource)
Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h=>h(App)
})

import axios from 'axios'
import qs from 'qs'
axios.interceptors.request.use(config => {    // 这里的config包含每次请求的内容
    // 判断localStorage中是否存在api_token
    if (localStorage.getItem('api_token')) {
        //  存在将api_token写入 request header
        config.headers.apiToken = `${localStorage.getItem('api_token')}`;
    }
    return config;
}, err => {
    return Promise.reject(err);
});

axios.interceptors.response.use(response => {
    return response
}, error => {
    return Promise.resolve(error.response)
});
//开发地址http://192.168.1.210:8086
//
// var baseUrl = "http://192.168.1.99:8087/api";
// var tagUrl='http://192.168.1.99:8089/api'
// var staUrl='http://192.168.1.99:8086:9002/'
// var fileUrl='http://192.168.1.99:8086:9001/api'
// var gateUrl ='http://192.168.1.95:9090'
//测试地址
// var baseUrl = "http://192.168.1.41:9002/api";
// var tagUrl='http://192.168.1.41:9005/api'
// var staUrl='http://192.168.1.41:9002/'
// var fileUrl='http://192.168.1.41:9001/api'
// var gateUrl ='http://192.168.1.41:9008'
//正式地址
var baseUrl = "http://47.106.141.239:8087/api";
var tagUrl='http://47.106.141.239:8082/api'
var staUrl='http://47.106.141.239:8087/'
var fileUrl='http://47.106.141.239:8086/api'
function checkStatus (response) {
    // 如果http状态码正常，则直接返回数据
    if (response && (response.status === 200 || response.status === 304 ||
            response.status === 400)) {
        return response.data
    }
    // 异常状态下，把错误信息返回去
    return {
        status: -404,
        msg: '网络异常'
    }
}
function checkCode (res) {
    // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
    if (res.status === -404) {
        alert(res.msg)
    }
    if (res.data && (!res.data.success)) {
        // alert(res.data.error_msg)
    }
    return res
}
// 请求方式的配置
export default {
	login (url, data) {  //  post
        return axios({
            method: 'post',
            baseURL:baseUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000,
            data: qs.stringify(data),
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    post (url, data) {  //  post
        return axios({
            method: 'post',
            baseURL:baseUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            data: qs.stringify(data),
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
		gatePost (url, data) {  // 道闸 post
				return axios({
						method: 'post',
						baseURL:gateUrl,
						//baseURL: baseUrl,
						url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
						data: qs.stringify(data),
						timeout: 100000,
						headers: {
								'X-Requested-With': 'XMLHttpRequest',
								'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
						}
				}).then(
						(response) => {
								return checkStatus(response)
						}
				).then(
						(res) => {
								return checkCode(res)
						}
				)
		},
		gateGet (url, params) {  // get
				return axios({
						method: 'get',
						baseURL:gateUrl,
						url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
						params, // get 请求时带的参数
						timeout: 100000,
						headers: {
								'X-Requested-With': 'XMLHttpRequest'
						}
				}).then(
						(response) => {
								return checkStatus(response)
						}
				).then(
						(res) => {
								return checkCode(res)
						}
				)
		},
    get (url, params) {  // get
        return axios({
            method: 'get',
            baseURL:baseUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    //针对tag的post方法
     tagPost (url, data) {  //  post
        return axios({
            method: 'post',
            baseURL:tagUrl,
            //baseURL: baseUrl,
           url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            data: qs.stringify(data),
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
     //针对tag的get方法
    tagGet(url, params) {  // get
        return axios({
            method: 'get',
            baseURL:tagUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    put (url, params) {  // get
        return axios({
            method: 'put',
            baseURL:baseUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    tagPut (url, params) {  // get
        return axios({
                method: 'put',
                baseURL:tagUrl,
                //baseURL: baseUrl,
                url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
                params, // get 请求时带的参数
                timeout: 100000,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(
                (response) => {
                return checkStatus(response)
            }
    ).then(
            (res) => {
            return checkCode(res)
        }
    )
    },
    delete (url, params) {  // get
        return axios({
            method: 'delete',
            baseURL: baseUrl,
            //baseURL: baseUrl,
           url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    tagDelete (url, params) {  // get
        return axios({
                method: 'delete',
                baseURL: tagUrl,
                url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
                params, // get 请求时带的参数
                timeout: 100000,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(
                (response) => {
                return checkStatus(response)
            }
    ).then(
            (res) => {
            return checkCode(res)
        }
    )
    },
    //针对static的get方法
    staGet(url, params) {  // get
        return axios({
            method: 'get',
            baseURL:staUrl,
            //baseURL: baseUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    //针对轮播图的批量删除
    picDelete (url, params) {  // get
        return axios({
            method: 'delete',
            baseURL: baseUrl,
            //baseURL: baseUrl,
            url: url,
            params, // get 请求时带的参数
            timeout: 100000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(
            (response) => {
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
    //上传学生和老师excel
    uploadFile (url, file, name, data) {
        var formData = new FormData();
        formData.append(name, file);
        for (var i in data) {
            formData.append(i, data[i]);
        }
        console.log(formData);
        return axios({
            method: 'POST',
            baseURL:fileUrl,
            url: url+'?'+Math.random()*100000+"&token="+JSON.parse(localStorage.getItem("uInfo")).token,
            data:formData,
            timeout: 100000,
        }).then(
            (response) => {
                console.log(response)
                return checkStatus(response)
            }
        ).then(
            (res) => {
                return checkCode(res)
            }
        )
    },
}

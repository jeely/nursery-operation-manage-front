import Vue from 'vue'
import Router from 'vue-router'
import Login from 'components/login/login'
import MainIndex from 'components/mainIndex'
import MainGates from 'components/mainGates/mainGates'
import GatesMana from 'components/mainGates/gatesMana'
import MainEducation from 'components/mainEducation/mainEducation'
import MainMember from 'components/mainMember/mainMember'
import MemberManage from 'components/mainMember/membermanage'
import MainPark from 'components/mainPark/mainPark'
import MainVideo from 'components/mainVideo/mainVideo'
import VideoDtl from 'components/mainVideo/videoDtl'
import UpdatePwd from 'components/updatePwd/updatePwd'
import Circles from 'components/circles/circles'
import AddCircles from 'components/circles/addCircle'
import EditCircles from 'components/circles/editCircle'
import TeachCloud from 'components/teachCloud/teachCloud'
import EditCloud from 'components/teachCloud/editCloud'
import AddResource from 'components/teachCloud/addResource'
import MainPicture from 'components/mainPicture/mainPicture'
import PictureAdd from 'components/mainPicture/pictureAdd'
import MainStatic from 'components/mainStatic/mainStatic'
import MassMessage from 'components/massMessage/massMessage'
import AddMessage from 'components/massMessage/addMessage'
import MassMessageDetails from 'components/massMessage/massMessageDetails'
import Gate from 'components/gate/gate'
import GateMan from 'components/gate/gateMan'

Vue.use(Router)

export default new Router({
	routes: [{
			path: "/",
			redirect: '/login'
		},
		{
			path: '/login',
			name: 'login',
			component: Login
		}, {
			path: '/updatePwd',
			name: 'updatePwd',
			component: UpdatePwd
		},
		{
			path: '/mainIndex',
			name: 'mainIndex',
			component: MainIndex,
			redirect: '/mainPark',
			children: [{
					path: '/mainPark',
					component: MainPark
				},
				{
					path: '/mainGates',
					component: MainGates
				},
				{
					path: '/gatesMana',
					component: GatesMana
				},
				{
					path: '/mainMember',
					component: MainMember
				},
				{
					path: '/memberManage',
					component: MemberManage
				},
				{
					path: '/mainEducation',
					component: MainEducation
				},
				{
					path: '/mainPicture',
					component: MainPicture
				},
				{
					path: '/mainStatic',
					component: MainStatic
				},
				{
					path: '/pictureAdd',
					component: PictureAdd
				},

				{
					path: '/mainVideo',
					component: MainVideo
				},
				{
					path: '/mainEducation',
					component: MainEducation
				},
				{
					path: '/mainVideo',
					component: MainVideo
				},
				{
					path: '/videoDtl',
					name: 'videoDtl',
					component: VideoDtl
				}, {
					path: '/circles',
					name: 'circles',
					component: Circles
				}, {
					path: '/addCircles',
					name: 'addCircles',
					component: AddCircles
				}, {
					path: '/editCircles',
					name: 'editCircles',
					component: EditCircles
				},
				{
					path: '/teachCloud',
					name: 'teachCloud',
					component: TeachCloud
				},
				{
					path: '/addResource',
					name: 'addResource',
					component: AddResource
				},
				{
					path: '/editCloud',
					name: 'editCloud',
					component: EditCloud
				},
				{
					path: '/massMessage',
					name: 'massMessage',
					component: MassMessage
				},
				{
					path: '/addMessage',
					name: 'addMessage',
					component: AddMessage
				},
				{
					path: '/massMessageDetails',
					name: 'massMessageDetails',
					component: MassMessageDetails
				},
				{
					path: '/gate',
					name: 'gate',
					component: Gate
				},{
					path: '/gateMan',
					name: 'gateMan',
					component: GateMan
				},

			]
		}
	]
})

/**
 * Created by zhiwei.shen on 2017/9/25.
 */
// 验证中文名称
function isChinaName(name) {
    var pattern = /^[\u4E00-\u9FA5]{2,6}$/;
    return pattern.test(name);
}

// 验证手机号
function isPhoneNo(phone) {
    var pattern = /^1[1-9]\d{9}$/;
    return pattern.test(phone);
}

// 验证身份证
function isCardNo(card) {
    var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return pattern.test(card);
}
//验证银行卡号
function isBank(num) {
    var pattern = /\d{15}|\d{19}/;
    if (pattern.test(num)||num=="") {
        return true;
    }else{
        return false;
    };
}
//身份证号合法性验证
//支持15位和18位身份证号
//支持地址编码、出生日期、校验位验证
function IdentityCodeValid(code) {
    var pass= true;

    if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
        pass = false;
    }
    else{
        //18位身份证需要验证最后一位校验位
        // if(code.length == 18){
        //     code = code.split('');
        //     //∑(ai×Wi)(mod 11)
        //     //加权因子
        //     var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
        //     //校验位
        //     var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
        //     var sum = 0;
        //     var ai = 0;
        //     var wi = 0;
        //     for (var i = 0; i < 17; i++)
        //     {
        //         ai = code[i];
        //         wi = factor[i];
        //         sum += ai * wi;
        //     }
        //     var last = parity[sum % 11];
        //     if(parity[sum % 11] != code[17]){
        //         pass =false;
        //     }
        // }
    }
    return pass;
}
/**
 * 验证密码6-20数字和字母组合
 * @param pw
 * @returns {boolean}
 */
function isPassword(pw) {
    var patrn=/^(\w){6,20}$/;
    return patrn.test(pw);}
//校验登录名：只能输入5-20个以字母开头、可带数字、“_”、“.”的字串
function isRegisterUserName(s)
{
    var patrn=/^[a-zA-Z0-9_-]{6,20}$/;
    if (!patrn.exec(s)) return false
    return true
}
/**
 *验证非空
 */
function isEmpty(parameter) {
    if(parameter==""||parameter==undefined||parameter==null)
        return false;
    if (parameter.length<1||parameter.length>10000){
        return false;
    }
    return true;
}
function  isedictor(parameter) {
    if (parameter.length<1){
        return false;
    }
    return true;
}
function CheckPhone(number){
    console.log(number)
    var reg = /^((0\d{2,3}-\d{7,8})|(1[3584]\d{9}))$/;
    return reg.test(number);
}
function isMessage(parameter) {
    if (parameter.length<1||parameter.length>200){
        return false;
    }
    return true;
}
function isAddress(address) {
    if (address.length<1||address.length>50){
        return false;
    }
    return true;
}
/**
 * 判断中文字符长度
 * @param parameter 参数
 * @param startLen 长度
 * @returns {boolean}
 */
function strLen(parameter,startLen,endLen) {
    if (parameter.length>endLen||parameter.length<startLen)
        return false;
    return true;
}
/**
 * 验证是否是金钱格式
 * @param money
 * @returns {boolean}
 */
function isMoney(money) {
    var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
    if (reg.test(money)||money=="") {
        return true;
    }else{
        return false;
    };
}
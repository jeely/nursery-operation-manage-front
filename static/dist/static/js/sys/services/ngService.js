/**
 * Created by Administrator on 2017/11/13.
 */
/**
 * @生成随机数
 * */
function randoma() {
    var num= Math.random()*100
    return parseInt(num);
}
if (!app.ngServer) app.ngServer = {};


//app.ngServer.host = "http://47.106.141.239:8087/api";
 //app.ngServer.host = "http://192.168.1.8:8087/api" ;
app.ngServer.host = "http://tex-nursery-operation.apps.aorise.org/api";
//请求
//@param type -- 请求类型
//@param $http -- ng $http
//@param url -- 请求url type：string
//@param params -- 请求参数  type：object
//@param callback -- 成功返回 type：function
//@param errorCallBack -- 错误返回 type：function
app.ngServer.ajax = function(type, $http, url, params, callback, errorCallBack, paramType) {
    var config = {
        method: ('' + type).toUpperCase(),
        url: app.ngServer.host + url,
        params: params
    }
    if ((type == "POST" && !paramType) || paramType == "DATA") {
        config = {
            method: ('' + type).toUpperCase(),
            url: app.ngServer.host + url,
            data: params
        }
    }
    $http(config).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}


app.ngServer.ajaxGet = function($http, url, params, callback, errorCallBack, paramType) {
    app.ngServer.ajax("GET", $http, url, params, callback, errorCallBack, paramType);
}

app.ngServer.ajaxPost = function($http, url, params, callback, errorCallBack, paramType) {
    app.ngServer.ajax("POST", $http, url, params, callback, errorCallBack, paramType);
}

app.ngServer.ajaxDelete = function($http, url, params, callback, errorCallBack, paramType) {
    app.ngServer.ajax("DELETE", $http, url, params, callback, errorCallBack, paramType);
}

app.ngServer.ajaxPut = function($http, url, params, callback, errorCallBack, paramType) {
    app.ngServer.ajax("PUT", $http, url, params, callback, errorCallBack, paramType);
}

app.ngServer.uploadFile = function(url, file, name, data, callback, errorCallBack) {
    var formData = new FormData();
    formData.append(name, file);
    for (var i in data) {
        formData.append(i, data[i]);
    }
    $.ajax({
        url: app.ngServer.host + url,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        beforeSend: function() {
            console.log("正在进行，请稍候");
        },
        success: function(responseStr) {
            if (callback) callback(responseStr);
            console.log("成功" + responseStr);
        },
        error: function(responseStr) {
            if (errorCallBack) errorCallBack(responseStr);
            console.log("失败");
        }
    });
}


/*put*/
app.ngServer.ajaxPute = function($http, url, data, callback, errorCallBack) {
    $http({
        method: "PUT",
        url: app.ngServer.host + url,
        data: data,
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}


/*post*/
app.ngServer.aPost = function($http, url, data, callback, errorCallBack) {
    $http({
        method: "POST",
        url: app.ngServer.host + url,
        data: data,
        headers:{'Content-Type': 'application/json;charset=UTF-8'}
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}

app.ngServer.ajPost = function($http, url, data, callback, errorCallBack) {
    $http({
        method: "POST",
        url: app.ngServer.host + url,
        params: data,
        headers:{'Content-Type': 'application/json;charset=UTF-8'}
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}
app.ngServer.ajPoste = function($http, url, formData, callback, errorCallBack) {

    $http({
        method: "POST",
        url: app.ngServer.host + url,
        // params: data,
        data:formData,
        processData: false,
        contentType: false,
        headers:{'Content-Type': undefined}
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}

app.ngServer.ajDelete = function($http, url, data, callback, errorCallBack) {
    $http({
        method: "delete",
        url: app.ngServer.host + url,
        params: data,
        headers:{'Content-Type': 'application/json;charset=UTF-8'}
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}
app.ngServer.delete = function($http, url, data, callback, errorCallBack) {
    $http({
        method: "delete",
        url: app.ngServer.host + url,
        data: data,
        headers:{'Content-Type': 'application/json;charset=UTF-8'}
    }).success(function(res) {
        if (callback) callback(res);
    }).error(function(res) {
        if (errorCallBack) errorCallBack();
    })
}

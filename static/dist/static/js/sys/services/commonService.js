/**
 * Created by Administrator on 2017/11/13.
 */
if (!app.commonServer) app.commonServer = {};
//设置消息提示关闭时间(毫秒)
var successMsgTime=1000;
var errorMsgTime=2000;
//ajax调用接口错误
app.commonServer.errorCallBack=function(){
    layer.msg('系统正忙,请稍后再试...',{time:successMsgTime});
}
//ajax调用接口成功
app.commonServer.callback=function(res){
    //返回状态 true-成功；false-失败
    var state=res.ret||false;
    //接口端状态码
    var code=res.code||'';
    //成功消息
    var successMsg=res.msg||'';
    //失败消息
    var failedMsg=res.message||'';
    if(state){
        layer.msg(successMsg,{time:successMsgTime});
    }else{
        layer.msg(failedMsg,{time:errorMsgTime});
    }
}
//ajax调用接口成功--查询用，查询成功不提示
app.commonServer.callbackQuery=function(res){
    //返回状态 true-成功；false-失败
    var state=res.ret||false;
    //接口端状态码
    var code=res.code||'';
    //成功消息
    var successMsg=res.msg||'';
    //失败消息
    var failedMsg=res.message||'';
    if(state){
        //layer.msg(successMsg);
    }else{
        layer.msg(failedMsg,{time:errorMsgTime});
    }
}

var app = angular.module('fpApp', ['ui.router']);
app.filter('cut', function () {         //过滤器 裁剪字符超出部分...
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});
app.config(['$provide', function ($provide) {
    $provide.decorator('ngClickDirective',['$delegate','$timeout', function ($delegate,$timeout) {
        var original = $delegate[0].compile;
        var delay = 3000;   //设置间隔时间
        $delegate[0].compile = function (element, attrs, transclude) {

            var disabled = false;
            function onClick(evt) {
                if (disabled) {
                    evt.preventDefault();
                    evt.stopImmediatePropagation();
                } else {
                    disabled = true;
                    $timeout(function () { disabled = false; }, delay, false);
                }
            }
            //   scope.$on('$destroy', function () { iElement.off('click', onClick); });
            element.on('click', onClick);

            return original(element, attrs, transclude);
        };
        return $delegate;
    }]);
}]);


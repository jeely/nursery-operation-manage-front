app.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
    $urlRouterProvider.otherwise('/login');
    $stateProvider
       //系统菜单配置开始
        .state('login', {
            url: '/login',
            templateUrl:'login.html',
            cache:'false'
        })
        .state('maintain', {
            url: '/maintain',
            templateUrl:'maintain.html',
            cache:'false'
        })
        .state('reset', {
            url: '/reset',
            templateUrl:'views/changePw.html',
            cache:'false'
        })
        .state("main", {
            url: "/main",
            templateUrl: 'main.html',
            cache:'false'
        })
        //系统菜单配置结束
        //二级菜单配置 -父ID为：1
        .state('main.park', {
            params:{'oneMenuId':1},//1:一级菜单id
            url: '/park',
            templateUrl:'views/park/park.html',
            cache:'false'
        })
        .state('main.member', {
            params:{'oneMenuId':2},
            url: '/member',
            templateUrl:'views/member/member.html',
            cache:'false'
        })
        .state('main.memberManage', {
            params:{'oneMenuId':3,'nLesseeDB':null,'name':null},
            url: '/member/memberManage?nLesseeDB=&name=',
            templateUrl:'views/member/memberManage.html',
            cache:'false'
        })
        .state('main.video', {
            params:{'oneMenuId':3},
            url: '/video',
            templateUrl:'views/video/video.html',
            cache:'false'
        })
        .state('main.manage', {
            params:{'oneMenuId':3,'id':null,'name':null,'nLesseeDB':null},
            url: '/video/manage?id=&name=&nLesseeDB=',
            templateUrl:'views/video/manage.html',
            cache:'false'
        })
        .state('main.gates', {
            params:{'oneMenuId':3},
            url: '/gates',
            templateUrl:'views/gates/gates.html',
            cache:'false'
        })
        .state('main.gatesMana', {
            params:{'oneMenuId':3,'id':null,'name':null},
            url: '/gates/gatesMana?id=&name=',
            templateUrl:'views/gates/gatesMana.html',
            cache:'false'
        })
        .state('main.education', {
            params:{'oneMenuId':3},
            url: '/education',
            templateUrl:'views/education/education.html',
            cache:'false'
        })
        //二级菜单配置 -父ID为：...
});
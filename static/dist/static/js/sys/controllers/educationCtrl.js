/**
 * 教育局模块控制器
 */
app.controller("educationCtrl", function($scope, $http) {
    $scope.education = {
        id:'',
        eUser: '',
        eDepartment: '',
        ePhone: '',
        eProvince: '',
        provinceCode:'',
        eCity:'',
        cityCode:'',
        eCounty:'',
        countyCode:'',
        pageIndex: 1,
        pageNum: 15
    }
    var refreshPaging = function(pagingInfo, res) {
        var getPages = function(totalPage) {
            var pages = [];
            for(var i = 0; i < totalPage; i++) {
                pages.push(i + 1);
            }
            return pages;
        }
        var data = {
            totalPages: getPages(res.data.totalPage),
            everyPage: res.data.everyPage,
            currentPage: res.data.currentPage,
            totalCount: res.data.totalCount,
        };
        pagingInfo = $.extend(pagingInfo, data);
    }
    $scope.pageConfig = {
        nextPage: function(page) {
            $scope.queryDate({
                pageIndex: page
            })
        },
        prvePage: function(page) {
            $scope.queryDate({
                pageIndex: page
            })
        },
        changePage: function(page) {
            $scope.queryDate({
                pageIndex: page
            })
        },
        totalPages: [],
        everyPage: 10,
        currentPage: 1,
        totalCount: 0
    };
    $scope.queryDate = function(config) {
        config = config || {};
        var data = $.extend($scope.education, config);
        console.log(data);
        var url = "/education/queryEducationList/pageIndex/" + data.pageIndex + "/pageNum/" + data.pageNum + "?token=" + $scope.token + '&' + $scope.randoms();
        console.log(url);
        app.ngServer.ajaxGet($http, url, data, function(res) {
            console.log(res.data)
            refreshPaging($scope.pageConfig, res);
            $scope.list = res.data.list || [];
        }, function() {
            $scope.list = [];
        });
    }
    $scope.queryDates = function() {
        $scope.education.pageIndex = 1;
        $scope.queryDate();
    }
    var init = function() {
        $scope.queryDate();
        $scope.queryArea(0, '0');
    }
    init();
    /*
     * @省市区查询方法重置
     * 注意：需要在ctr中定义cityCode，countyCode
     * */
    $scope.resetArea = function(i) {
        switch(i) {
            case 0:
                break;
            case 1:
                $scope.education.cityCode = '';
                $scope.education.countyCode = '';
                break;
            case 2:
                $scope.education.countyCode = '';
                break;
            default:
                break;
        }
    };
    /*
     * @省市区编辑方法重置
     * 注意：需要在ctr中定义cityCode，countyCode
     * */
    $scope.resetAreas = function(i) {
        switch(i) {
            case 0:
                break;
            case 1:
                $scope.educations.cityCode = '';
                $scope.educations.countyCode = '';
                break;
            case 2:
                $scope.educations.countyCode = '';
                break;
            case 3:
                $scope.educations.cityCode = '';
            default:
                break;
        }
    };
    /*
     * @新增教育局
     *
     * */
    $scope.isAdd = false;
    $scope.addModel = function() {
        $("#addModel").modal('show');
        $scope.isDis = false;
        $scope.isDist = false;
        $scope.isDisb = false;
        $scope.isAdd = true;
        $scope.educations = {
            province: '',
            city: '',
            county: '',
            provinceCode: '',
            cityCode: '',
            countyCode: '',
            dept: '',
            userName: '',
            phone: '',
            createUser:$scope.userId
        }
        $scope.queryArea(1,"")
        console.log($scope.educations)
    };
    $scope.dataValidate = function() {
        if(!isEmpty($scope.educations.provinceCode) || !isEmpty($scope.educations.cityCode) || !isEmpty($scope.educations.countyCode)) {
            layer.tips("请选择所属区域", "#eCountyCode");
            return false;
        }
        if(!checkChar($scope.educations.dept, 2, 30, 2)) {
            layer.tips("请输入正确的部门名称", "#eDepartment");
            return false;
        }
        if(!checkChar($scope.educations.userName, 2, 30, 2)) {
            layer.tips("使用者姓名为2-30个字符", "#eUser");
            return false;
        }
        if(!checkChar($scope.educations.phone,11, 11, 4)) {
            layer.tips("请输入正确的联系方式", "#ePhone");
            return false;
        }
        return true;
    }
    $scope.cancel = function() {
        $("#addModel").modal('hide');
        $scope.queryArea(1,"");
    }
    $scope.add = function() {
        if(!$scope.dataValidate()) {
            return;
        }
        var data = $scope.educations;
        data.province = $scope.areasName;
        data.city = $scope.cityName;
        data.county = $scope.townsName;
        if($scope.isAdd == true) {
            var url = "/education/addEducation" + "?token=" + $scope.token;
            app.ngServer.ajPost($http, url, data, function(res) {
                console.log(res)
                app.commonServer.callback(res)
                layer.msg(res.msg||res.message);
                if(res.ret == true) {
                    $("#addModel").modal('hide');
                    $scope.queryDate()
                }
                 $scope.errorState(res)
            }, function() {
                app.commonServer.errorCallBack;
            })
        } else {
            var url = '/education/editEducation' + "?token=" + $scope.token;
            app.ngServer.ajaxPut($http, url, data, function(res) {
                console.log(res)
                app.commonServer.callback(res)
                layer.msg(res.msg||res.message)
                if(res.ret == true) {
                    $("#addModel").modal('hide');
                    $scope.queryDate()
                } else {
                    console.log(2)
                }
                 $scope.errorState(res)
            }, function() {
                app.commonServer.errorCallBack;
            })
        }
    }
    /*
     * @根据id查询教育局信息
     * */
    $scope.editModel = function(id) {
        var url = "/education/queryEducation/?id=" + id + "&token=" + $scope.token + '&' + $scope.randoms();;
        console.log(id)
        app.ngServer.ajaxGet($http, url, {}, function(res) {
            console.log(res)
            $scope.educations = {
                id:id,
                province: res.data.eProvince,
                city:res.data.eCity,
                county: res.data.eCounty,
                provinceCode: res.data.eProvinceCode,
                cityCode: res.data.eCityCode,
                countyCode: res.data.eCountyCode,
                dept:res.data.eDepartment,
                userName:res.data.eUser,
                phone:res.data.ePhone,
                createUser:res.data.createUser,
                editTime:res.data.editTime
            };
            $scope.queryArea(1, res.data.eProvinceCode);
            $scope.queryArea(2, res.data.eCityCode);
            $scope.queryArea(3, res.data.eCountyCode);
            if(res.ret == true) {
                $("#addModel").modal('show');
                $scope.isAdd = false;
            }
        }, function() {
            $scope.list = [];
        });
    }
    /*
     * @重置密码/激活
     * */
    $scope.reset = function(phone) {
        layer.open({
            title: '重置密码',
            content: '初始密码为123456，确定将密码重置吗？',
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function() {
                var url = "/education/resetPassword";
                var data = {
                    phone: phone,
                    editUser: $scope.userId,
                    token: $scope.token
                }
                app.ngServer.ajaxPut($http, url, data, function(res) {
                    if(res.ret = true) {
                        $scope.queryDate();
                        layer.msg('重置成功');
                    }
                }, function() {

                });
            },
            no: function() {
                layer.open({
                    content: '您选择了取消',
                    time: 1
                });
            }
        });
    }
    /*
     * @删除教育局信息
     * */
    $scope.delete = function(id) {
        layer.open({
            content: '确定删除账号吗？',
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function() {
                var url = "/education/delEducation/?id=" + id + "&token=" + $scope.token;
                var data = {
                    editUser: $scope.userId
                }
                app.ngServer.ajaxDelete($http, url, data, function(res) {
                    if(res.ret == true) {
                        $scope.queryDate();
                        layer.msg('删除成功');
                    } else {
                        layer.msg(res.msg || res.message)
                    }
                }, function() {

                });
            },
            no: function() {
                layer.open({
                    content: '您选择了取消',
                    time: 1
                });
            }
        });
    }
})
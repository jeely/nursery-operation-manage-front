/**
 * Created by Administrator on 2017/8/15.
 */


//登录
app.controller('loginCtrl', function($scope, $http,$state,locals) {
    /*----------登录--------*/
    $scope.login = {
        userName: "",
        pwd: ""
    }
    $scope.isActive = true;
    $scope.dataValidate=function ()  {
        if (!isRegisterUserName($scope.login.userName)){
            layer.tips("请输入正确的用户名","#userName");
            return false;
        }
        if (!isPassword($scope.login.pwd)){
            layer.tips("请输入正确的密码","#password");
            return false;
        }
        return true;
    }
    $scope.submit = function(config) {
        if (!$scope.dataValidate()){
            return;
        }
        config = config || {};
        var data = $.extend($scope.login, config);
        $scope.isActive = true;
        var url = "/login";
        app.ngServer.ajPost($http, url, data, function (res) {
            if(res.ret == true){
                locals.set("userName", res.data.userName);
                locals.set("id",res.data.id);
                locals.set("token",res.data.token);
                $state.go("main.park");
                layer.msg("登录成功");
            }else{
                layer.msg(res.msg||res.message);
                $scope.login.passWord = "";
            }
        }, function () {
            layer.msg('请求超时');
        })
    };
    $scope.rem=function (i) {
        if(i==1){
            locals.set("name", $scope.login.userName);
        }else{
            localStorage.removeItem("name");
        }
    }
    $scope.name = locals.get("name","");
    if(!$scope.name ){
        $scope.login={
            userName:"",
            pwd: ""
        };
        $scope.rememberPsd=0
    }else{
        $scope.login.userName=$scope.name;
        $scope.rememberPsd=1;
    }
    $scope.myKeyup = function(e){
        var keycode = window.event?e.keyCode:e.which;
        if(keycode==13){
            $scope.submit()
        }
    };

});



app.controller("indexCtrl", function($scope, $http) {

	/**
	 * @生成随机数
	 * */
	$scope.randoms = function() {
		var num = Math.random() * 100000
		return parseInt(num);
	}
})

/*修改密码*/
app.controller('passWordCtrl', function($scope, $state, $http, locals) {
	$scope.oldPW = locals.get("oldPW", "");
	$scope.reset = {
		userId: $scope.userId,
		userName: $scope.userName,
		oldPassword: '',
		newPassword: '',
		confirmPW: ''
	};
	$scope.dataValidate = function() {
		if(!isPasd($scope.reset.newPassword)) {
			layer.msg("密码不符合要求");
			angular.element('#npwEI').focus();
			return false;
		}
		if($scope.reset.confirmPW != $scope.reset.newPassword) {
			layer.msg("两次密码不一致");
			angular.element('#cfmpwEI').focus();
			return false;
		}
		if($scope.reset.oldPassword != $scope.oldPW) {
			layer.msg("原始密码不正确");
			angular.element('#oldpwEI').focus();
			return false;
		}
		if($scope.reset.newPassword == $scope.oldPW) {
			layer.msg("新密码不能与原始密码相同");
			angular.element('#npwEI').focus();
			return false;
		}
		return true;
	}
	$scope.modifyPW = function() {
		if(!$scope.dataValidate()) {
			return;
		}
		var url = "/monitor/changePassword/userId/" + $scope.reset.userId,
			data = {
				oldPassword: $scope.reset.oldPassword,
				newPassword: $scope.reset.newPassword
			};
		app.ngServer.ajaxPut($http, url, data, function(res) {
			if(res.ret == true) {
				layer.msg("修改成功");
				localStorage.removeItem("oldPW");
				locals.set("oldPW", $scope.reset.newPassword);
				$state.go("main")
			} else {
				layer.msg(res.msg || res.message);
				$scope.reset.oldPassword = "";
			}
		}, function() {
			layer.msg('请求超时');
		})
	};
});

app.controller('defaultCtrl', function($scope, $state, locals, $rootScope, $filter, $interval, $http) {
	$scope.focus = 1;
	var hashList = location.hash.split('/'),
		urlOne = hashList[hashList.length - 1],
		urlTwo = hashList[hashList.length - 2];
	$scope.urlFocus = function() {
		if(urlOne == 'park') {
			$scope.focus = 1;
		}
		if(urlOne == 'member' || urlTwo == 'member') {
			$scope.focus = 2;
		}
		if(urlOne == 'video' || urlTwo == 'video') {
			$scope.focus = 3;
		}
		if(urlOne == 'gates' || urlTwo == 'gates') {
			$scope.focus = 4;
		}
		if(urlOne == 'education') {
			$scope.focus = 5;
		}
	};
	$scope.goIndex = function() {
		$scope.focus = 1;
		$state.go("main.park")
	}
	$scope.urlFocus()
	$scope.chooseLi = function(i) {
		$scope.focus = i;
	}
	$("*").bind("keydown", function(event) {
		if(event.keyCode == 13) {
			$('form').each(function() {
				event.preventDefault();
			});
		}
	});
	/*
	 * 系统错误跳转页面
	 * */
	$scope.errorState = function(res) {
		if(res.code == 5001) {
			$state.go("maintain")
		}
	}
	/*
	 * 获取登录成功数据
	 * */
	$scope.userName = locals.get("userName", "");
	$scope.userId = locals.get("id", "");
	$scope.token = locals.get("token", "");

	/*
	 * @省市区查询方法
	 * 定义省市区
	 * */
	$scope.province = {};
	$scope.city = {};
	$scope.towns = {};
	$scope.townes = {};
	$scope.queryArea = function(i, code) {
		switch(i) {
			case 0:
				$scope.city = {};
				$scope.towns = {};
				$scope.townes = {}
				break;
			case 1:
				$scope.city = {};
				$scope.towns = {};
				$scope.townes = {}
				break;
			case 2:
				$scope.towns = {};
				$scope.townes = {}
				break;
			case 3:
				$scope.townes = {}
				break;
			default:
				break;
		}
		if(code.length < 1) {
			return;
		};
		var url = "/AreaInfo/queryArea/code/" + code + "?token=" + $scope.token;
		$scope.queryCityName(i, code);
		app.ngServer.ajaxGet($http, url, {}, function(res) {
			switch(i) {
				case 0:
					$scope.areas = res.data;
					break;
				case 1:
					$scope.city = res.data;
					break;
				case 2:
					$scope.towns = res.data;
					break;
				case 3:
					$scope.townes = res.data;
					break;
				default:
					break;
			}
		}, function() {

		});
	};
	/**
	 * @根据代号查询城市名字方法
	 * */
	$scope.queryCityName = function(i, code) {
		var url = '/AreaInfo/queryAreaName/code/' + code + "?token=" + $scope.token;
		app.ngServer.ajaxGet($http, url, {}, function(res) {
			switch(i) {
				case 0:
					break;
				case 1:
					$scope.areasName = res.data.aName;
					break;
				case 2:
					$scope.cityName = res.data.aName;
					break;
				case 3:
					$scope.townsName = res.data.aName;
					break;
				default:
					$scope.townesName = res.data.aName;
					break;
			}
		}, function() {

		});
	}

	$scope.permissionQuery = function() {
		var token = locals.get("token", "");
		if(!token) {
			$state.go('login')
		}
	};
	$scope.permissionQuery()

	$scope.outUser = function() { //注销
		var url = "/logout" + "?token=" + $scope.token;
		app.ngServer.ajaxGet($http, url, {}, function(res) {
			if(res.ret == true) {
				localStorage.removeItem("userName");
				localStorage.removeItem("id");
				localStorage.removeItem("token");
				$state.go("login");
				layer.msg("注销成功");
			}
		}, function() {
			layer.msg('请求超时');
		})
	}
	/**
	 * @修改密码
	 * */
	$scope.reset = {
		id: $scope.userId,
		pwd: '',
		oldpwd: '',
		userId: $scope.userId,
		token: $scope.token
	};
	$scope.newPwd = '';
	$scope.dataValidate = function() {
		if(!isEmpty($scope.reset.oldpwd)) {
			layer.tips("请输入原密码", "#oldpsd");
			return false;
		}
		if(!isEmpty($scope.reset.pwd)) {
			layer.tips("请输入新密码", "#psd");
			return false;
		}
		if(!isEmpty($scope.newPwd)) {
			layer.tips("请再次输入新密码", "#newPsd");
			return false;
		}
		if($scope.reset.pwd != $scope.newPwd) {
			layer.tips("新密码两次输入不一致,请重新填写！", "#newPsd");
			return false;
		}
		if($scope.reset.pwd == $scope.reset.oldpwd) {
			layer.msg("新密码不能与原始密码相同");
			return false;
		}
		//5-16位密码验证
		if(!isPassword($scope.reset.pwd)) {
			layer.tips("密码不符合规范(长度5-16,只能含大小写字母、数字、符号)", "#psd");
			return false;
		}
		return true;
	}
	$scope.resetPsd = function() {
		if(!$scope.dataValidate()) {
			return;
		}
		var url = '/updatePassword';
		var data = $scope.reset;
		console.log(data);
		app.ngServer.ajaxPut($http, url, data, function(res) {
			console.log(res);
			if(res.ret) {
				layer.msg('修改成功');
				localStorage.removeItem("userName");
				localStorage.removeItem("id");
				localStorage.removeItem("token");
				$state.go('login');
			} else {
				layer.msg(res.message);
			}

		}, function() {
			layer.msg('请求超时');
		})
	}

})
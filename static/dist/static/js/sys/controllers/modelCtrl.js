/**
 * Created by lyj on 2018/4/4.
 * 模块控制器
 */
/**
 * 幼儿园模块控制器
 */
app.controller("parkCtrl", function($scope, $http) {
	$scope.park = {
		nName: '',
		nProvince: '',
		nCity: '',
		nCounty: '',
		pageIndex: 1,
		pageNum: 15,
		nProvinceCode: '',
		nCityCode: '',
		nCountyCode: ''
	}
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
		pagingInfo = $.extend(pagingInfo, data);
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function(config) {
		config = config || {};
		var data = $.extend($scope.park, config);
		console.log(data);
		var url = "/SysNursery/pageIndex/" + data.pageIndex + "/pageNum/" + data.pageNum + "?token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res.data)
			refreshPaging($scope.pageConfig, res);
			$scope.list = res.data.list || [];
		}, function() {
			$scope.list = [];
		});
	}
	$scope.queryDates = function() {
		$scope.park.pageIndex = 1;
		$scope.queryDate()
	}
	var init = function() {
		$scope.queryDate();
		$scope.queryArea(0, '0');
	}
	init();
	/*
	 * @省市区查询方法重置
	 * 注意：需要在ctr中定义nCityCode，nCountyCode，nCountyCode
	 * */
	$scope.resetArea = function(i) {
		switch(i) {
			case 0:
				break;
			case 1:
				$scope.park.nCityCode = '';
				$scope.park.nCountyCode = '';
				break;
			case 2:
				$scope.park.nCountyCode = '';
				break;
			default:
				break;
		}
	};
	/*
	 * @省市区查询方法重置
	 * 注意：需要在ctr中定义nCityCode，nCountyCode，nCountyCode
	 * */
	$scope.resetAreas = function(i) {
		switch(i) {
			case 0:
				break;
			case 1:
				$scope.parks.nCityCode = '';
				$scope.parks.nCountyCode = '';
				$scope.parks.nTownCode = '';
				break;
			case 2:
				$scope.parks.nCountyCode = '';
				$scope.parks.nTownCode = '';
				break;
			case 3:
				$scope.parks.nTownCode = '';
				break;
			default:
				break;
		}
	};
	/*
	 * @新增幼儿园
	 * */
	$scope.isAdd = false;
	$scope.one = '';
	$scope.two = '';
	$scope.addModel = function() {
		$("#addModel").modal('show');
		$scope.isDis = false;
		$scope.isDist = false;
		$scope.isDisb = false;
		$scope.isAdd = true;
		$scope.parks = {
			nName: '',
			nAccount: '',
			nAccountOne: '',
			nAccountTwo: '',
			nLeader: '',
			nPhone: '',
			nProvince: '',
			nCity: '',
			nCounty: '',
			nProvinceCode: '',
			nCityCode: '',
			nCountyCode: '',
			nAddress: '',
			nLongitude: '',
			nLatitude: '',
			nTownCode: '',
			nTown: '',
			userId: $scope.userId,
		}
        $scope.queryArea(1,"")
		console.log($scope.parks)
	};
	$scope.dataValidate = function() {
		if(!checkChar($scope.parks.nName, 2, 30, 2)) {
			layer.tips("请输入正确的园所名称", "#nName");
			return false;
		}
		if(!checkChar($scope.parks.nAccount, 5, 16, 1)) {
			layer.tips("后台账号为5-16个字符", "#nAccount");
			return false;
		}
		if(isEmpty($scope.parks.nAccountOne)) {
			if(!checkChar($scope.parks.nAccountOne, 5, 16, 1)) {
				layer.tips("后台账号为5-16个字符", "#nAccountOne");
				return false;
			}
		}
		if(isEmpty($scope.parks.nAccountTwo)) {
			if(!checkChar($scope.parks.nAccountTwo, 5, 16, 1)) {
				layer.tips("后台账号为5-16个字符", "#nAccountTwo");
				return false;
			}
		}
		if($scope.parks.nAccountOne == $scope.parks.nAccount) {
			layer.tips("子账号不能与主账号相同", "#nAccountOne");
			return false;
		}
		if($scope.parks.nAccountTwo == $scope.parks.nAccount) {
			layer.tips("子账号不能与主账号相同", "#nAccountTwo");
			return false;
		}
		if(isEmpty($scope.parks.nAccountOne) && isEmpty($scope.parks.nAccountTwo)) {
			if($scope.parks.nAccountTwo == $scope.parks.nAccountOne) {
				layer.msg("子账号不能相同");
				return false;
			}
		}
		if(!checkChar($scope.parks.nLeader, 2, 30, 3)) {
			layer.tips("请输入正确的园长姓名", "#nLeader");
			return false;
		}
		if(!checkChar($scope.parks.nPhone, 11, 11, 4)) {
			layer.tips("请输入正确的联系方式", "#nPhone");
			return false;
		}
		if(!isEmpty($scope.parks.nTownCode)) {
			layer.tips("请选择所属区域", "#nTownCode");
			return false;
		}
		if(isEmpty($scope.parks.nAddress)) {
			if(!checkChar($scope.parks.nAddress, 1, 100)) {
				layer.tips("请填写详细地址", "#nAddress");
				return false;
			}
		}
		return true;
	}
	$scope.cancel = function() {
		$("#addModel").modal('hide');
        $scope.queryArea(1,"");
	}
	$scope.add = function() {
		if(!$scope.dataValidate()) {
			return;
		}
		var data = $scope.parks;
		data.nProvince = $scope.areasName;
		data.nCity = $scope.cityName;
		data.nCounty = $scope.townsName;
		data.nTown = $scope.townesName;
		data.nLatitude = document.getElementById("latitude").value;
		data.nLongitude = document.getElementById("longitude").value
		if($scope.isAdd == true) {
			var url = "/sysNursery" + "?token=" + $scope.token;
			app.ngServer.ajPost($http, url, data, function(res) {
				console.log(res)
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#addModel").modal('hide');
					$scope.queryDate()
				}
				/* $scope.errorState(res)*/
			}, function() {
				app.commonServer.errorCallBack;
			})
		} else {
			var url = '/updateSysNursery' + "?token=" + $scope.token;
			delete data.nAccount;
			if($scope.one != '' && $scope.one == data.nAccountOne) {
				delete data.nAccountOne;
			}
			if($scope.two != '' && $scope.two == data.nAccountTwo) {
				delete data.nAccountTwo;
			}
			console.log(data)
			app.ngServer.ajaxPut($http, url, data, function(res) {
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#addModel").modal('hide');
					$scope.queryDate()
				} else {
					console.log(2)
					$scope.parks.nAccount = $scope.moren;
					$scope.parks.nAccountOne = $scope.one;
					$scope.parks.nAccountTwo = $scope.two
				}
				/* $scope.errorState(res)*/
			}, function() {
				$scope.parks.nAccount = $scope.moren;
				$scope.parks.nAccountOne = $scope.one;
				$scope.parks.nAccountTwo = $scope.two;
				app.commonServer.errorCallBack;
			})
		}

	}
	/*
	 * @根据id查询幼儿园信息
	 * */

	$scope.editModel = function(id) {
		var url = "/sysNursery/id/" + id + "?token=" + $scope.token + '&' + $scope.randoms();
		console.log(id)
		app.ngServer.ajaxGet($http, url, {}, function(res) {
			console.log(res)
			if(res.ret == true) {
				$scope.parks = {
					nurseryId: id,
					nName: res.data.nName,
					nAccount: res.data.nAccount,
					nAccountOne: res.data.nAccountOne,
					nAccountTwo: res.data.nAccountTwo,
					nLeader: res.data.nLeader,
					nPhone: res.data.nPhone,
					nProvince: res.data.nProvince,
					nCity: res.data.nCity,
					nCounty: res.data.nCounty,
					nProvinceCode: res.data.nProvinceCode,
					nCityCode: res.data.nCityCode,
					nCountyCode: res.data.nCountyCode,
					nAddress: res.data.nAddress || '',
					userId: $scope.userId,
					nTownCode: res.data.nTownCode,
					nTown: res.data.nTown,
					nLongitude: res.data.nLongitude,
					nLatitude: res.data.nLatitude,
					nAccountId: res.data.accountId || '',
					nAccountOneId: res.data.accountOneId || '',
					nAccountTwoId: res.data.accountTwoId || ''
				};
				$scope.moren = res.data.nAccount
				$scope.one = res.data.nAccountOne;
				$scope.two = res.data.nAccountTwo;
				if(!$scope.parks.nAccount) {
					$scope.isDis = false
				} else {
					$scope.isDis = true;
				};
				if(!$scope.parks.nAccountOne) {
					$scope.isDist = false
				} else {
					$scope.isDist = true;
				};
				if(!$scope.parks.nAccountTwo) {
					$scope.isDisb = false
				} else {
					$scope.isDisb = true;
				}
				$scope.queryArea(1, res.data.nProvinceCode);
				$scope.queryArea(2, res.data.nCityCode);
				$scope.queryArea(3, res.data.nCountyCode);
				$scope.queryArea(4, res.data.nTownCode);
				$("#addModel").modal('show');
				$scope.isAdd = false;
			}else{
				layer.msg(res.msg)
			}
		}, function() {
			$scope.list = [];
		});
	}
	/*
	 * @重置密码/激活
	 * */
	$scope.reset = function(id) {
		layer.open({
			title: '重置密码',
			content: '初始密码为12345678，确定将密码重置吗？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/updatePwd";
				var data = {
					nurseryId: id,
					userId: $scope.userId,
					token: $scope.token
				}
				app.ngServer.ajaxPut($http, url, data, function(res) {
					if(res.ret = true) {
						$scope.queryDate();
						layer.msg('重置成功');
					}
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}
	$scope.rest = function() {
		layer.open({
			title: '重置密码',
			content: '初始密码为123456，确定将密码重置吗？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/education/resetPassword";
				var data = {
					phone: $scope.parks.nPhone,
					editUser: $scope.userId,
					token: $scope.token
				}
				app.ngServer.ajaxPut($http, url, data, function(res) {
					if(res.ret = true) {
						layer.msg('重置成功,新密码为123456');
					}
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}
	$scope.active = function(id, state) {
		layer.open({
			title: '冻结/启用',
			content: '确定要冻结/启用吗？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/setActivatedState";
				if(state == 1) {
					var data = {
						nurseryId: id,
						state: state + 1,
						token: $scope.token,
						userId: $scope.userId
					}
				} else {
					var data = {
						nurseryId: id,
						state: state - 1,
						token: $scope.token,
						userId: $scope.userId
					}
				}
				app.ngServer.ajaxPut($http, url, data, function(res) {
					if(res.ret = true) {
						$scope.queryDate();
						layer.msg('状态修改成功');
					}
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}

})
/**
 * 会员管理模块控制器
 */
app.controller("memberCtrl", function($scope, $http) {
	$scope.member = {
		name: '',
		pageIndex: 1,
		pageNum: 15,
		nProvinceCode: '',
		nCityCode: '',
		nCountyCode: ''
	}
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
		pagingInfo = $.extend(pagingInfo, data);
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function(config) {
		config = config || {};
		var data = $.extend($scope.member, config);
		var url = "/member/query/pageIndex/" + data.pageIndex + "/pageNum/" + data.pageNum + "?token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res.data)
			refreshPaging($scope.pageConfig, res);
			$scope.list = res.data.list || [];
		}, function() {
			$scope.list = [];
		});

	}
	$scope.queryDates = function() {
		$scope.member.pageIndex = 1;
		$scope.queryDate()
	}
	var init = function() {
		$scope.queryDate();
		$scope.queryArea(0, '0')
	}
	init();
	/*
	 * @省市区查询方法重置
	 * 注意：需要在ctr中定义nCityCode，nCountyCode，nCountyCode
	 * */
	$scope.resetArea = function(i) {
		switch(i) {
			case 0:
				break;
			case 1:
				$scope.member.nCityCode = '';
				$scope.member.nCountyCode = '';
				break;
			case 2:
				$scope.member.nCountyCode = '';
				break;
			default:
				break;
		}
	};

})

/**
 * 视频模块控制器
 */
app.controller("videoCtrl", function($scope, $http) {
	$scope.video = {
		nName: '',
		nCity: '',
		nCounty: '',
		nProvince: '',
		pageIndex: 1,
		pageNum: 15,
		nProvinceCode: '',
		nCityCode: '',
		nCountyCode: ''
	}
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
		pagingInfo = $.extend(pagingInfo, data);
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function(config) {
		config = config || {};
		var data = $.extend($scope.video, config);
		var url = "/SysNursery/pageIndex/" + data.pageIndex + "/pageNum/" + data.pageNum + "?token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res.data)
			refreshPaging($scope.pageConfig, res);
			$scope.list = res.data.list || [];
		}, function() {
			$scope.list = [];
		});

	}
	$scope.queryDates = function() {
		$scope.video.pageIndex = 1;
		$scope.queryDate()
	}
	var init = function() {
		$scope.queryDate();
		$scope.queryArea(0, '0')
	}
	init();
	/*
	 * @省市区查询方法重置
	 * 注意：需要在ctr中定义nCityCode，nCountyCode，nCountyCode
	 * */
	$scope.resetArea = function(i) {
		switch(i) {
			case 0:
				break;
			case 1:
				$scope.video.nCityCode = '';
				$scope.video.nCountyCode = '';
				break;
			case 2:
				$scope.video.nCountyCode = '';
				break;
			default:
				break;
		}
	};

})
/**
 * 视频管理子模块控制器
 */
app.controller("manageCtrl", function($scope, $http, $state) {
	$scope.nurseryId = $state.params.id;
	$scope.nurseryName = $state.params.name;
	$scope.nurseryDB = $state.params.nLesseeDB;
	$scope.options = [];
	/*
	 * @根据id查询幼儿园视频信息
	 * */
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
		pagingInfo = $.extend(pagingInfo, data);
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function() {
		var url = "/video" + '?' + $scope.randoms();
		var data = {
			nLesseeDb: $scope.nurseryDB,
			token: $scope.token
		}
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res)
			refreshPaging($scope.pageConfig, res);
			$scope.list = res.data || [];
		}, function() {
			$scope.list = [];
		});
	}
	/*
	 * @查询班级下拉框
	 * */
	$scope.queryClass = function() {
		var url = "/class?nLesseeDb=" + $scope.nurseryDB + "&token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, {}, function(res) {
			console.log(res)
			$scope.options = res.data || [];
		}, function() {
			$scope.list = [];
		});
	}
	var init = function() {
		$scope.queryDate();
		$scope.queryClass();
	}
	init();
	/*
	 * @新增
	 * */

	$scope.chooseAdd = function() {
		$("#manageModel").modal('show');
		$scope.manage = {
			nurseryId: $scope.nurseryId,
			groupId: '0',
			vLocation: '',
			vParameter: '',
			createUser: $scope.userId,
			nLesseeDb: $scope.nurseryDB
		}
		$scope.isAdd = true;
	}
	/**
	 * 数据验证
	 */
	$scope.dataValidate = function() {
		if($scope.manage.groupId == 0) {
			layer.tips("请选择区域", "#classId");
			return false;
		}
		if(!checkChar($scope.manage.vLocation, 2, 30, -1)) {
			layer.tips("请输入正确的点位", "#vLocation");
			return false;
		}
		if(!isEmpty($scope.manage.vParameter)) {
			layer.tips("请输入值", "#vParameter");
			return false;
		}
		return true;
	}
	$scope.add = function() {
		if(!$scope.dataValidate()) {
			return;
		}
		var data = $.extend($scope.manage, {});
		var url = '/video' + "?token=" + $scope.token;
		if($scope.isAdd == true) {
			console.log(data)
			app.ngServer.ajPost($http, url, data, function(res) {
				console.log(res)
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#manageModel").modal('hide');
					$scope.queryDate()
				}
				/* $scope.errorState(res)*/
			}, function() {
				app.commonServer.errorCallBack;
			})
		} else {
			app.ngServer.ajaxPut($http, url, data, function(res) {
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#manageModel").modal('hide');
					$scope.queryDate()
				}
				/* $scope.errorState(res)*/
			}, function() {
				app.commonServer.errorCallBack;
			})
		}

	}
	$scope.cancel = function() {
		$("#manageModel").modal('hide');
	}
	/*
	 * @查看幼儿园视频信息
	 * */
	$scope.amend = function(i) {
		var url = "/video/id/" + i + '?' + $scope.randoms();
		var data = {
			nLesseeDb: $scope.nurseryDB,
			token: $scope.token
		}
		app.ngServer.ajaxGet($http, url, data, function(res) {
			$scope.manage = {
				id: i,
				groupId: res.data.groupId.toString(),
				vLocation: res.data.vLocation,
				vParameter: res.data.vParameter,
				editUser: $scope.userId,
				nLesseeDb: $scope.nurseryDB
			};
			if(res.ret = true) {
				$("#manageModel").modal('show');
				$scope.isAdd = false;
			}
		}, function() {
			$scope.list = [];
		});
	}
	/*
	 * @删除幼儿园视频信息
	 * */
	$scope.delete = function(id) {
		layer.open({
			title: '删除',
			content: '是否删除？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/video/id/" + id + "?token=" + $scope.token;
				var data = {
					nLesseeDb: $scope.nurseryDB,
					editUser: $scope.userId
				}
				app.ngServer.ajaxDelete($http, url, data, function(res) {
					if(res.ret == true) {
						$scope.queryDate();
						layer.msg('删除成功');
					} else {
						layer.msg(res.msg || res.message)
					}
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}
	/*
	 * 测试连接
	 * */
	$scope.test = function(id) {
		layer.open({
			title: '测试连接',
			content: '测试连接？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/video/test?" + $scope.randoms();
				var data = {
					nLesseeDb: $scope.nurseryDB,
					token: $scope.token,
					id: id
				}
				app.ngServer.ajaxGet($http, url, data, function(res) {
					layer.msg(res.msg || res.message)
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}
})

/**
 * 门禁模块控制器
 */
app.controller("gatesCtrl", function($scope, $http) {
	$scope.video = {
		nName: '',
		nCity: '',
		nCounty: '',
		nProvince: '',
		pageIndex: 1,
		pageNum: 15,
		nProvinceCode: '',
		nCityCode: '',
		nCountyCode: ''
	}
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
		pagingInfo = $.extend(pagingInfo, data);
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function(config) {
		config = config || {};
		var data = $.extend($scope.video, config);
		var url = "/SysNursery/pageIndex/" + data.pageIndex + "/pageNum/" + data.pageNum + "?token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res.data)
			refreshPaging($scope.pageConfig, res);
			$scope.list = res.data.list || [];
		}, function() {
			$scope.list = [];
		});

	}
	$scope.queryDates = function() {
		$scope.video.pageIndex = 1;
		$scope.queryDate()
	}
	var init = function() {
		$scope.queryDate();
		$scope.queryArea(0, '0')
	}
	init();
	/*
	 * @省市区查询方法重置
	 * 注意：需要在ctr中定义nCityCode，nCountyCode，nCountyCode
	 * */
	$scope.resetArea = function(i) {
		switch(i) {
			case 0:
				break;
			case 1:
				$scope.video.nCityCode = '';
				$scope.video.nCountyCode = '';
				break;
			case 2:
				$scope.video.nCountyCode = '';
				break;
			default:
				break;
		}
	};

})
/**
 * 门禁管理子模块控制器
 */
app.controller("gatesManaCtrl", function($scope, $http, $state) {
	$scope.nurseryId = $state.params.id;
	$scope.nurseryName = $state.params.name;
	$scope.options = [];
	/*
	 * @根据id查询幼儿园门禁信息
	 * */
	var refreshPaging = function(pagingInfo, res) {
		var getPages = function(totalPage) {
			var pages = [];
			for(var i = 0; i < totalPage; i++) {
				pages.push(i + 1);
			}
			return pages;
		}
		var data = {
			totalPages: getPages(res.data.totalPage),
			everyPage: res.data.everyPage,
			currentPage: res.data.currentPage,
			totalCount: res.data.totalCount,
		};
	}

	$scope.pageConfig = {
		nextPage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		prvePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		changePage: function(page) {
			$scope.queryDate({
				pageIndex: page
			})
		},
		totalPages: [],
		everyPage: 10,
		currentPage: 1,
		totalCount: 0
	};
	$scope.queryDate = function() {
		var url = "/gates/getGatesList" + '?' + $scope.randoms();
		var data = {
			nurseryId: $scope.nurseryId,
			token: $scope.token
		}
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res)
			$scope.list = res.data || [];
		}, function() {
			$scope.list = [];
		});
	}

	var init = function() {
		$scope.queryDate();
	}
	init();
	/*
	 * @新增
	 * */

	$scope.chooseAdd = function() {
		$("#manageModel").modal('show');
		$scope.manage = {
			id: '0',
			deviceType: '1',
			deviceNum: '',
			deviceDes: '',
			createUser: $scope.userId,
			nurseryId: $scope.nurseryId
		}
		$scope.isAdd = true;
	}
	$scope.dataValidate = function() {
		if(!checkChar($scope.manage.deviceNum, 2, 100, -1)) {
			layer.tips("请输入正确的设备编号", "#number");
			return false;
		}
		if(!checkChar($scope.manage.deviceDes, 2, 100, -1)) {
			layer.tips("请输入设备描述", "#describe");
			return false;
		}
		return true;
	}
	$scope.add = function() {
		if(!$scope.dataValidate()) {
			return
		}
		var data = $.extend($scope.manage, {});
		if($scope.isAdd == true) {
			var url = '/gates/addGates' + "?token=" + $scope.token;
			console.log(url)
			console.log(data)
			app.ngServer.ajPost($http, url, data, function(res) {
				console.log(res)
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#manageModel").modal('hide');
					$scope.queryDate()
				}
				/* $scope.errorState(res)*/
			}, function() {
				app.commonServer.errorCallBack;
			})
		} else {
			var url = '/gates/editGates' + "?token=" + $scope.token;
			app.ngServer.ajaxPut($http, url, data, function(res) {
				app.commonServer.callback(res);
				if(res.ret == true) {
					$("#manageModel").modal('hide');
					$scope.queryDate()
				}
				/* $scope.errorState(res)*/
			}, function() {
				app.commonServer.errorCallBack;
			})
		}

	}
	$scope.cancel = function() {
		$("#manageModel").modal('hide');
	}
	/*
	 * @查看幼儿园门禁信息
	 * */
	$scope.amend = function(i) {
		var url = "/gates/getGates/" + '?' + $scope.randoms();
		var data = {
			id: i,
			token: $scope.token
		}
		app.ngServer.ajaxGet($http, url, data, function(res) {
			$scope.manage = {
				id: i,
				deviceType: res.data.type.toString(),
				deviceNum: res.data.number,
				deviceDes: res.data.describe,
				editUser: $scope.userId
			};
			if(res.ret = true) {
				$("#manageModel").modal('show');
				$scope.isAdd = false;
			}
		}, function() {
			$scope.list = [];
		});
	}
	/*
	 * @删除幼儿园门禁信息
	 * */
	$scope.delete = function(id) {
		layer.open({
			title: '删除',
			content: '是否删除？',
			btn: ['确认', '取消'],
			shadeClose: false,
			yes: function() {
				var url = "/gates/delGates?token=" + $scope.token;
				var data = {
					id: id,
					editUser: $scope.userId,
					token: $scope.token
				}
				app.ngServer.ajaxDelete($http, url, data, function(res) {
					if(res.ret == true) {
						$scope.queryDate();
						layer.msg('删除成功');
					} else {
						layer.msg(res.msg || res.message)
					}
				}, function() {

				});
			},
			no: function() {
				layer.open({
					content: '您选择了取消',
					time: 1
				});
			}
		});
	}
})

/*
 * @会员管理子模块控制器
 * */
app.controller("memberManageCtrl", function($scope, $http, $state) {
	$scope.nurseryDB = $state.params.nLesseeDB;
	$scope.nurseryName = $state.params.name;
	/*
	 * @根据id查询会员信息
	 * */
	$scope.mem = {
		nurseryDB: $scope.nurseryDB,
		gradeId: '',
		classId: '',
		showUnVip: ''
	}
	$scope.queryDate = function(config) {
		config = config || {};
		var data = $.extend($scope.mem, config);
		var url = "/member/queryStudent" + "?token=" + $scope.token + '&' + $scope.randoms();
		app.ngServer.ajaxGet($http, url, data, function(res) {
			$scope.list = res.data.list || [];
			if(res.data.list.length > 0) {
				$scope.unVipSum = res.data.list[0].unVipSum
			} else {
				$scope.unVipSum = 0
			}
		}, function() {
			$scope.list = [];
		});
	}
	/**
	 * @查询班级年级
	 * */
	$scope.getGrade = function() {
		var data = {
			nLesseeDb: $scope.nurseryDB,
			token: $scope.token
		}
		var url = '/grade';
		app.ngServer.ajaxGet($http, url, data, function(res) {
			$scope.grades = res.data || [];
		}, function() {

		});
	}
	$scope.queryName = function(i, e) {
		var url = '/gradeclass' + '?' + $scope.randoms();
		if(i == 0) {
			var data = {
				gradeId: e,
				nLesseeDb: $scope.nurseryDB,
				token: $scope.token
			}
			app.ngServer.ajaxGet($http, url, data, function(res) {
				$scope.gradeNames = res.data.gName;
			}, function() {

			});
		} else {
			var data = {
				classId: e,
				nLesseeDb: $scope.nurseryDB,
				token: $scope.token
			}
			app.ngServer.ajaxGet($http, url, data, function(res) {
				$scope.classNames = res.data.cName;
			}, function() {

			});
		}

	}
	$scope.changeGrade = function(e) {
		$scope.mem.classId = '';
		$scope.classNames = null;
		var data = {
			nLesseeDb: $scope.nurseryDB,
			token: $scope.token,
			gradeId: e
		};
		$scope.queryName(0, e);
		var url = '/classByGid';
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res)
			$scope.classes = res.data || [];
		}, function() {
			$scope.list = [];
		});
	}
	var init = function() {
		$scope.getGrade()
		$scope.queryDate()
	}
	init();
	/*
	 * @批量设置
	 * */
	$scope.sitting = {
		vipDate: '',
		gradeId: '',
		classId: '',
		showUnVip: '',
		nurseryDB: $scope.nurseryDB
	}
	$scope.chooseAdd = function() {
		$("#manageModel").modal('show');
		$scope.sitting.isVip = 0;
		$scope.sitting.vipDate = '';
		$scope.vipDateList = getVipDateList()
	}
	/*
	 * @打开设置单个学生提示框
	 * */
	/*
	 * @比较两个日期大小方法
	 * */
	function dateSize(a, b) {
		a = a.substring(0, 10)
		var resDay = new Date(a.replace(/\-/g, "\/"));
		var today = new Date(b.replace(/\-/g, "\/"));
		if(resDay < today) {
			$scope.sit.isVip = 0;
		} else {
			$scope.sit.isVip = 1;
		}
	}
	$scope.dataSizes = function(a) {
		if(!a) {
			return '否'
		} else {
			var resDay = new Date(a.replace(/\-/g, "\/"));
			var day = getDateStr3(date);
			var today = new Date(day.replace(/\-/g, "\/"));
			if(resDay < today) {
				return '否'
			} else {
				return '是'
			}
		}

	}
	var date = new Date();

	$scope.editStu = function(id) {
		var data = {
			nurseryDB: $scope.nurseryDB,
			stuId: id
		}
		var url = "/member/queryStuDetials" + "?token=" + $scope.token + '&' + $scope.randoms();;
		app.ngServer.ajaxGet($http, url, data, function(res) {
			console.log(res.data)
			$scope.sit = {
				tname: res.data.tname,
				sGender: res.data.sGender == '1' ? '男' : '女',
				isVip: res.data.sVip,
				stuId: id,
				nurseryDB: $scope.nurseryDB,
				sNumber: res.data.sNumber,
				sCardNumber: res.data.sCardNumber,
				vipDate: res.data.sVipValidity.substring(0, 10),
				gName: res.data.gName,
				className: res.data.className,
				vipDateList: getVipDateList()
			};
			console.log($scope.sit);
			if(res.ret == true) {
				$("#stuModel").modal('show');
				$scope.isAdd = false;
				dateSize(res.data.sVipValidity, getDateStr3(date))
			}
		}, function() {
			$scope.list = [];
		});
	}

	function getVipDateList() {
		var vipDateListJson = [];
		var now = date;
		var year = now.getFullYear();
		var month = (now.getMonth() + 1);

		for(var i = year; i < year + 3; i++) {
			var vipDateItem1;
			var vipDateItem2;
			if(month < 7) {
				vipDateItem1 = {
					code: i + "-07-15",
					name: i + "年7月15"
				};
				vipDateItem2 = {
					code: (i + 1) + "-02-15",
					name: (i + 1) + "年2月15"
				};
			} else {
				vipDateItem1 = {
					code: (i + 1) + "-02-15",
					name: (i + 1) + "年2月15"
				};
				vipDateItem2 = {
					code: (i + 1) + "-07-15",
					name: (i + 1) + "年7月15"
				};
			}
			vipDateListJson.push(vipDateItem1);
			vipDateListJson.push(vipDateItem2);
		}
		var vipDateItem = {
			code: "1900-01-01",
			name: "取消会员"
		};
		vipDateListJson.push(vipDateItem);
		console.log(vipDateListJson);
		return vipDateListJson;
	}
	/*
	 * @设置单个学生vip状态
	 * */
	$scope.oneSit = function() {
		var data = {
			nurseryDB: $scope.nurseryDB,
			stuId: $scope.sit.stuId,
			isVip: $scope.sit.isVip,
			vipDate: $scope.sit.vipDate
		};
		if(!$scope.sit.vipDate) {
			layer.msg("请选择会员有效期");
			return;
		}
		var url = '/member/setStuVip' + "?token=" + $scope.token;
		app.ngServer.ajaxPut($http, url, data, function(res) {
			console.log(res)
			app.commonServer.callback(res);
			if(res.ret = true) {
				$("#stuModel").modal('hide');
				$scope.queryDate()
			}
			/* $scope.errorState(res)*/
		}, function() {
			app.commonServer.errorCallBack;
		})
	}
	/*
	 * @批量修改学生vip
	 * */
	$scope.add = function() {

		var data = {
			nurseryDB: $scope.nurseryDB,
			gradeId: $scope.mem.gradeId,
			classId: $scope.mem.classId,
			showUnVip: $scope.mem.showUnVip,
			vipDate: $scope.sitting.vipDate
		};
		console.log($scope.sitting.vipDate)
		if(!$scope.sitting.vipDate) {
			layer.msg("请选择会员有效期");
			return;
		}
		var url = '/member/batchSetVip' + "?token=" + $scope.token;
		app.ngServer.ajaxPut($http, url, data, function(res) {
			console.log(res)
			app.commonServer.callback(res);
			if(res.ret == true) {
				$("#manageModel").modal('hide');
				$scope.queryDate()
			}
			/* $scope.errorState(res)*/
		}, function() {
			app.commonServer.errorCallBack;
		})
	}
	$scope.cancel = function() {
		$("#manageModel").modal('hide');
		$("#stuModel").modal('hide')
	}
})
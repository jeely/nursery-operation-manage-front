app.controller('timeCtrl', function($scope, $interval) {
				var date = new Date();
				var year = date.getFullYear();
				var month = date.getMonth();
				var day = date.getDate();
				var mon = parseInt(month) + 1;

				$interval(function() {
					var y = new Date().getHours();
					var z = new Date().getMinutes();
					var a = new Date().getSeconds();
					var aa = checkTime(a);
					var zz = checkTime(z);

					function checkTime(i) {
						if(i < 10) {
							i = '0' + i;
						} else {
							i = i;
						}
						return i;
					}
					$scope.time = y + ':' + zz + ':' + aa;
					$scope.date = year+"-"+mon+"-"+day;
				}, 1000);

			})
/*时间控件*/
app.controller("dateCtrl",function ($scope) {
    /*比较两个日期大小*/
    function checkEndTime(){
        var startTime=$("#startTime").val();
        var start=new Date(startTime.replace("-", "/").replace("-", "/"));
        var endTime=$("#endTime").val();
        var end=new Date(endTime.replace("-", "/").replace("-", "/"));
        if(end<start){
            return false;
        }
        return true;
    }

    $('#startTime').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
        endDate:new Date()
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime").datepicker('setStartDate',null);
        };
        $("#endTime").val('')
    });
    $('#endTime').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true,
        endDate:new Date()
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
        if(checkEndTime()){

        }else{
            layer.msg("结束时间必须大于开始时间！")
            $("#startTime").val('')
        }
    });

    function checkEndTime2(){
        var startTime=$("#startTime2").val();
        var start=new Date(startTime.replace("-", "/").replace("-", "/"));
        var endTime=$("#endTime2").val();
        var end=new Date(endTime.replace("-", "/").replace("-", "/"));
        if(end<start){
            return false;
        }
        return true;
    }

    $('#startTime2').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime2").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime2").datepicker('setStartDate',null);
        };
        $("#endTime2").val('')
    });
    $('#endTime2').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
        if(checkEndTime2()){

        }else{
            layer.msg("结束时间必须大于开始时间");
            $("#startTime2").val('')
        }
    });

    $('#start').datepicker({
        format: 'yyyy-mm',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime").datepicker('setStartDate',null);
        };
    });
    $('#end').datepicker({
        format: 'yyyy-mm',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
    });
    $('#startTime_1').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime1").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime1").datepicker('setStartDate',null);
        };
    });
    $('#endTime_1').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
    });
    $("#datepicker").datepicker({maxDate:new Date()});
   // $("#endTime").datepicker({maxDate:new Date()});
})
/**
 * Created by Administrator on 2017/8/30.
 * lyj
 */
app.controller("dateCtrl",function ($scope) {
    $('#startTime').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime").datepicker('setStartDate',null);
        };
    });
    $('#endTime').datepicker({
        format: 'yyyy-mm-dd',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
    });
    $('#start').datepicker({
        format: 'yyyy-mm',
        language: "zh-CN",
        autoclose: true,
        pickerPosition:"top-right",
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $("#endTime").datepicker('setStartDate', new Date(ev.date.valueOf()));
        }else{
            $("#endTime").datepicker('setStartDate',null);
        };
    });
    $('#end').datepicker({
        format: 'yyyy-mm',
        language: "zh-CN",
        autoclose: true,
        clearBtn:true,
    }).on('changeDate', function(ev){
        if(ev.date){
            $(".startDate").datepicker('setEndDate', new Date(ev.date.valueOf()));
        }else{
            $(".startDate").datepicker('setEndDate',new Date());
        }
    });
})

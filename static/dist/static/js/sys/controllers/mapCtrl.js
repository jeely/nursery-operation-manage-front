app.controller('mapCtrl', function($scope) {
	// 百度地图API功能
	var map = new BMap.Map("boxMap"); // 创建Map实例
	map.centerAndZoom(new BMap.Point(110.002765, 27.583358), 11); // 初始化地图,设置中心点坐标和地图级别
	map.addControl(new BMap.MapTypeControl()); //添加地图类型控件
	map.setCurrentCity("怀化市"); // 设置地图显示的城市 此项是必须设置的
	map.enableScrollWheelZoom(true); //开启鼠标滚轮缩放
	map.addControl(new BMap.OverviewMapControl()); //添加默认缩略地图控件
	map.addEventListener("click", function(e) {
		 map.clearOverlays(); //清空原来的标注
		map.addOverlay(new BMap.Marker(new BMap.Point(e.point.lng, e.point.lat)));
		$scope.parks.nLongitude=e.point.lng,
		$scope.parks.nLatitude=e.point.lat
	});
	function createMk(){
		 map.clearOverlays(); //清空原来的标注
		var point =  new BMap.Point($scope.parks.nLongitude,$scope.parks.nLatitude);
		var mk = new BMap.Marker(point);
		map.addOverlay(mk);
		 mk.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
	}
	map.setMapStyle({
		styleJson: [{
				"featureType": "poi",
				"elementType": "all",
				"stylers": {
					"color": "#ffffff",
					"visibility": "off"
				}
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": {
					"color": "#ffffff",
					"visibility": "off"
				}
			}
		]
	});



	/**
	 * @打开map定位
	 * */
	$scope.addMap = function() {
		$("#addModel").modal('hide');
		$("#mapModel").modal('show');
		createMk()
	}
	$scope.adds = function() {
		$("#mapModel").modal('hide');
		$("#addModel").modal('show');
	}
	$scope.cancels=function(){
		$("#mapModel").modal('hide');
		$("#addModel").modal('show');
		$scope.parks.nLongitude="",
		$scope.parks.nLatitude=""
	}
})
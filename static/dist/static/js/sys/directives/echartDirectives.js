/**
 * Created by Administrator on 2017/9/1.
 * lyj
 */
app.directive('echartSource',function(){
    return{
        restrict:'AE',
        scope :{
            echartSource:'='
        },
        template:'<div></div>',
        link:function(scope,element,attr){
            var my_Chart = echarts.init(element[0]);
            // 双向传值
            scope.$watch('echartSource', function(n, o) {
                if (n === o || !n) return;
                my_Chart.setOption(n);
            });

            //当浏览器窗口发生变化的时候调用div的resize方法
            window.addEventListener('resize', chartResize);

            scope.$on('$destory', function() {
                window.removeEventListener('resize', chartResize);
            })

            function chartResize() {
                my_Chart.resize();
            }
        }
    };
})



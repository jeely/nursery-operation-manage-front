/**
 * Created by Administrator on 2017/11/13.
 */
app.directive('paging', ['$compile', function($compile) {
    return {
        restrict: 'E',
        template: '<div class="paging" ng-class="{true: \'hide\', false: \'\'}[config.totalPages.length <= 0]">\
                        <div class="paging-info">当前显示 {{config.currentPage == 1 ? 1 : ((config.currentPage-1) * config.everyPage) + 1}} 到 {{((config.currentPage * config.everyPage) > config.totalCount ? config.totalCount : (config.currentPage * config.everyPage))}} 条 , 共 {{config.totalCount}} 条  共 {{config.totalPages.length}} 页</div>\
                        <div class="paging-skip"><input type="text" id="idPagingInput" class="inputPage"/><button ng-click="toPageIndex()">跳转</button></div>\
                        <div class="dataTables_paginate paging_bootstrap">\
                            <ul class="pagination">\
                                <li ng-class="{true: \'disabled\', false: \'\'}[config.currentPage <= 1]" ng-click="sP()"><a href="javascript:void(0);">首页</a></li>\
                                <li class="prev" ng-class="{true: \'disabled\', false: \'\'}[config.currentPage <= 1]" ng-click="prveP()"><a href="javascript:void(0);">上一页</a></li>\
                                <li ng-repeat="(index,page) in config.totalPages" ng-if="config.currentPage > 9 ? (index < (config.currentPage+5) && index > config.currentPage-9) : (index < 10)" ng-click="changeP(page)" ng-class="{true: \'active\', false: \'\'}[index == (config.currentPage-1)]"><a href="javascript:void(0);">{{page}}</a></li>\
                                <li class="next"  ng-class="{true: \'disabled\', false: \'\'}[config.currentPage >= config.totalPages.length]"  ng-click="nextP()">\<a href="javascript:void(0);">下一页</a></li>\
                                <li ng-class="{true: \'disabled\', false: \'\'}[config.currentPage >= config.totalPages.length]" ng-click="mP()"><a href="javascript:void(0);">末页</a></li>\
                            </ul>\
                        </div>\
                    </div>',
        replace: true,
        scope: {
            config: "="
        },
        link: function(scope, element, attrs) {
            scope.nextP = function nextP() {
                if (scope.config.currentPage >= scope.config.totalPages.length) return;
                scope.config.nextPage(scope.config.currentPage + 1);
            }
            scope.sP = function sP() {
                if (scope.config.currentPage == 1) return;
                scope.config.nextPage(1);
            }
            scope.mP = function mP() {
                if (scope.config.currentPage == scope.config.totalPages.length) return;
                console.log(scope.config.totalPages.length);
                scope.config.nextPage(scope.config.totalPages.length);
            }
            scope.prveP = function prveP() {
                if (scope.config.currentPage <= 1) return;
                scope.config.nextPage(scope.config.currentPage - 1);
            }
            scope.changeP = function(page) {
                scope.config.changePage(page);
            }
            scope.toPageIndex = function(){
                var index = parseInt($(element).find("#idPagingInput").val()) || 0;
                if(index<0||index>scope.config.totalPages.length){
                    index = 1;
                    $(element).find("#idPagingInput").val(1)
                }
                scope.config.changePage(index);
            }
        }
    }
}])